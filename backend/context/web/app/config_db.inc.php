<?php
define('DB_TYPE', 'postgres');
define('DB_HOST', $_SERVER['DB_HOST']);
define('DB_NAME', $_SERVER['DB_DATABASE']);
define('DB_USER', $_SERVER['DB_USERNAME']);
define('DB_PASS', $_SERVER['DB_PASSWORD']);
define('DB_TABLE_PREFIX', '');