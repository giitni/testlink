<?php
require 'install-db-utils.php';

$conn = getConnection($_SERVER['DB_HOST'], $_SERVER['FORWARD_DB_PORT'], $_SERVER['DB_DATABASE'], $_SERVER['DB_USERNAME'], $_SERVER['DB_PASSWORD']);
$query = $conn->query('select * from public.users');
if ($query->rowCount() > 0) {
    echo 'no migration to run';
    exit(0);
}

foreach (['tables', 'udf0', 'default_data'] as $migration) {
    $conn->exec(file_get_contents("${argv[1]}/install/sql/postgres/testlink_create_${migration}.sql"));
}

$conn->exec("
  DELETE FROM users;
  INSERT INTO users (`login`, `password`, `role_id`, `email`, `first`, `last`, `locale`, `active`, `cookie_string`) 
  VALUES ('${_SERVER['TL_ADMIN']}', MD5('${_SERVER['TL_ADMIN_PASSWD']}'),  8, '${_SERVER['TL_ADMIN_EMAIL']}',  'Testlink', 'Administrator',  'pt_BR', 1, CONCAT(MD5(RAND()), MD5('${_SERVER['TL_ADMIN']}')))
");
