<?php
function getConnection($host, $port, $database, $user, $password) {
    $connTries = 0;
    $connMaxTries = 10;
    while ($connTries < $connMaxTries) {
        sleep(2);
        try {
            return new PDO("pgsql:host=${host};port=${port};dbname=${database}", $user, $password);
        } catch (Exception $e) {
            if (++$connTries === $connMaxTries) {
                echo "It wasn't possible to connect to database ${_SERVER['DB_HOST']}. Reason: ", $e->getMessage();
                exit(1);
            }
        }
    }
}